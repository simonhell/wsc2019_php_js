-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Erstellungszeit: 04. Jan 2020 um 18:30
-- Server-Version: 10.4.10-MariaDB
-- PHP-Version: 7.2.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Datenbank: `wsc_php_js`
--

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `attendee_registrations`
--

CREATE TABLE `attendee_registrations` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `event_id` int(10) UNSIGNED NOT NULL,
  `registration_type` enum('early_bird','general','vip') COLLATE utf8mb4_unicode_ci NOT NULL,
  `calculated_price` double NOT NULL,
  `event_rating` int(11) DEFAULT NULL,
  `registration_date` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Daten für Tabelle `attendee_registrations`
--

INSERT INTO `attendee_registrations` (`id`, `user_id`, `event_id`, `registration_type`, `calculated_price`, `event_rating`, `registration_date`, `updated_at`) VALUES
(1, 1, 1, 'general', 500, 1, '2019-12-13 11:50:55', '2019-12-13 11:50:59'),
(2, 9, 1, 'early_bird', 0, 1, '2020-01-03 13:24:32', '2020-01-03 14:10:06'),
(3, 9, 1, 'early_bird', 425, 2, '2020-01-03 13:25:15', '2020-01-03 13:25:15'),
(4, 9, 1, 'vip', 600, 0, '2020-01-03 13:25:26', '2020-01-03 13:25:26'),
(5, 9, 1, 'general', 500, 2, '2020-01-03 13:25:32', '2020-01-03 13:25:32'),
(6, 9, 1, 'general', 500, 1, '2020-01-03 13:27:09', '2020-01-03 13:27:09'),
(7, 9, 1, 'general', 500, 2, '2020-01-03 14:14:02', '2020-01-03 14:14:02'),
(8, 9, 1, 'early_bird', 425, 0, '2020-01-03 14:14:14', '2020-01-03 14:14:14');

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `events`
--

CREATE TABLE `events` (
  `id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `description` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `date` date NOT NULL,
  `time` time NOT NULL,
  `duration_days` smallint(5) UNSIGNED NOT NULL,
  `location` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `standard_price` double NOT NULL,
  `capacity` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Daten für Tabelle `events`
--

INSERT INTO `events` (`id`, `title`, `description`, `date`, `time`, `duration_days`, `location`, `standard_price`, `capacity`) VALUES
(1, 'Web conference', 'Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.', '2019-08-15', '08:00:00', 1, 'Floor1', 500, 250),
(2, 'Fishing experience', 'Lorem ipsum dolor sit amet, sadipscing consetetur elitr, sed diam nonumy eirmod tempor invidunt ut labore et dolore magna aliquyam erat, sed diam voluptua.', '2019-08-30', '08:00:00', 1, 'Garden Area', 100, 30);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Daten für Tabelle `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2019_08_19_000000_create_failed_jobs_table', 1),
(3, '2019_12_13_075709_create_attendee_registrations', 2),
(4, '2019_12_13_110937_add_time_to_sessions', 3);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `sessions`
--

CREATE TABLE `sessions` (
  `id` int(10) UNSIGNED NOT NULL,
  `event_id` int(10) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `room` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `speaker` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Daten für Tabelle `sessions`
--

INSERT INTO `sessions` (`id`, `event_id`, `title`, `room`, `speaker`) VALUES
(1, 1, 'CSS applied at 8:30', 'R05', 'Mac Entyre'),
(2, 1, 'JS advanced at 10:00', 'R06', 'Ann Codelle'),
(3, 2, 'fishing in troubled waters', NULL, NULL),
(4, 2, 'preparing fish for dish', NULL, NULL);

-- --------------------------------------------------------

--
-- Tabellenstruktur für Tabelle `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `username` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `firstname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(80) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_admin` tinyint(1) NOT NULL DEFAULT 0,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `photo` blob DEFAULT NULL,
  `linkedin_url` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Daten für Tabelle `users`
--

INSERT INTO `users` (`id`, `username`, `firstname`, `lastname`, `token`, `is_admin`, `email`, `photo`, `linkedin_url`, `password`) VALUES
(1, 'hellios11', 'Simon', 'Hell', NULL, 1, 'simon@deckweiss.at', NULL, 'https://deckweiss.at', '$2y$10$uVtLpecYru6Fil1K.kTr0OO4o3a.BgXHcUJTCj7lI4a98uyvPPSPS'),
(3, 'hellios1234', 'Simon', 'Hell', NULL, 0, 'simodn@deckweiss.at', NULL, 'someText', '$2y$10$7BzaEJ7Y2Z6JBS9G7Sg6BONae3FETClNWC3cPwokcvQOWOZpsMJQy'),
(9, 'smon', 'someText', 'someText', 'e3a8f67aaf54d806ad35ba525b970b06', 0, 'simon@text.at', 0x757365722d696d616765732f392e6a7067, 'someText', '$2y$10$lFM4XipoTgsKOiBdsj5dh.4XdSt/FS.7GC3DBkLPigUyTX6gPQ2QS'),
(19, 'smon', 'someText', 'someText', 'd674ca0e334bb67e5ad28934d6d3a54f', 0, 'simo123asdfn@text.at', 0x757365722d696d616765732f31392e6a7067, 'someText', '$2y$10$E187TjbjGc15FFxs6jzP9uVyf.ws1OY1NbInPQuQ875E4oXfoWcIe'),
(20, 'attendee1', 'someText', 'someText', NULL, 0, 'attendee1@a.at', 0x757365722d696d616765732f32302e6a7067, 'someText', '$2y$10$II.H7ooOF5FoZlim2fDLDeoussyZMKawUkKIQmNawhsQn6CcPy10u'),
(21, 'adminuser1', 'someText', 'someText', '1dec01141f9bfe969491af6e63ae1a14', 0, 'admin1@a.at', 0x757365722d696d616765732f32312e6a7067, 'someText', '$2y$10$6Gh7nrL/dMdDpI4KS2zsde4b8/l5dK8XFuk7iwO9ss2Ds8KjYchPy'),
(26, 'adminuser1112', 'someText', 'someText', '126b777fb790718fb42b1cb77231585d', 0, 'admin121231@a.at', 0x7075626c69632f757365722d696d616765732f32362e6a7067, 'someText', '$2y$10$JXuIaCpEjng0wom/IKPmseBaI.njD8Rqe4dV4PwCIMOASb7KMH8U6');

--
-- Indizes der exportierten Tabellen
--

--
-- Indizes für die Tabelle `attendee_registrations`
--
ALTER TABLE `attendee_registrations`
  ADD PRIMARY KEY (`id`),
  ADD KEY `attendee_registrations_user_id_foreign` (`user_id`),
  ADD KEY `attendee_registrations_event_id_foreign` (`event_id`);

--
-- Indizes für die Tabelle `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indizes für die Tabelle `sessions`
--
ALTER TABLE `sessions`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sessions_event_id_foreign` (`event_id`);

--
-- Indizes für die Tabelle `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD UNIQUE KEY `users_token_unique` (`token`);

--
-- AUTO_INCREMENT für exportierte Tabellen
--

--
-- AUTO_INCREMENT für Tabelle `attendee_registrations`
--
ALTER TABLE `attendee_registrations`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT für Tabelle `events`
--
ALTER TABLE `events`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT für Tabelle `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT für Tabelle `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT für Tabelle `sessions`
--
ALTER TABLE `sessions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT für Tabelle `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=27;

--
-- Constraints der exportierten Tabellen
--

--
-- Constraints der Tabelle `attendee_registrations`
--
ALTER TABLE `attendee_registrations`
  ADD CONSTRAINT `attendee_registrations_event_id_foreign` FOREIGN KEY (`event_id`) REFERENCES `events` (`id`),
  ADD CONSTRAINT `attendee_registrations_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`);

--
-- Constraints der Tabelle `sessions`
--
ALTER TABLE `sessions`
  ADD CONSTRAINT `sessions_event_id_foreign` FOREIGN KEY (`event_id`) REFERENCES `events` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
