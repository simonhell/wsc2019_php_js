<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class AttendeeRegistration extends Model
{
    protected $table="attendee_registrations";
    protected $guarded =[];
    protected $hidden = ['updated_at'];
    public const CREATED_AT = 'registration_date';
}
