<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
    protected $hidden = [];
    protected $guarded = ['updated_at', 'created_at'];
    public $timestamps = false;

    public function sessions() {
        return $this->hasMany('App\Session');
    }

    public function ratings() {
        return $this->hasMany(AttendeeRegistration::class,'event_id');
    }
}
