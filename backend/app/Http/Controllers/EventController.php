<?php

namespace App\Http\Controllers;

use App\Event;
use App\Session;
use App\User;
use Illuminate\Http\Request;

class EventController extends Controller
{


    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $events = Event::all();
        foreach ($events as $event) {
            $event->date = date_format(date_create($event->date), "d.m.Y");
        }
        return view('events.index')->with('events', $events);
    }

    public function apiIndex()
    {
        return Event::with('sessions')->get();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('events.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $event = new Event($request->except('sessions'));

        $event->save();
        if (isset($request->sessions)) {
            foreach ($request->sessions as $session) {
                $sessionEntity = new Session($session);
                $event->sessions()->create($sessionEntity->toArray());
            }

        }
        return redirect()->route('events.show', ['event' => $event->id])->with('message', 'Event successfully created');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $event = Event::find($id);
        return view('events.detail')->with('event', $event)->with('message', 'Event successfully created');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $event = Event::find($id);
        return view('events.edit')->with('event', $event)->with('eventSessions', json_encode($event->sessions));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $event = Event::find($id);
        $event->update($request->except('sessions'));
        if ($event->sessions) {
            foreach ($event->sessions as $session) {
                $session->delete();
            }

        }
        if ($request->sessions) {
            foreach ($request->sessions as $session) {
                $session = new Session([
                    'id' => $session['id'],
                    'time' => $session['time'],
                    'title' => $session['title'],
                    'room' => $session['room'],
                    'speaker' => $session['speaker'],
                ]);
                $event->sessions()->create($session->toArray());
            }
        }
        return redirect()->route('events.show', ['event' => $event->id])->with('message', 'Event successfully saved');
    }

    public function showAttendeeList()
    {
        return view('events.attendees',['users' => User::all()]);
    }

    public function showEventRating(Request $request, Event $event)
    {
        return view('events.rating', ['ratings' => $event->ratings->filter(function ($rating) {
            return isset($rating->event_rating);
        })]);
    }
}
