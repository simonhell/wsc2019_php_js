<?php

namespace App\Http\Controllers;

use App\AttendeeRegistration;
use App\User;
use Illuminate\Http\Request;
use App\Event;

class RegistrationController extends Controller
{
    const MESSAGE = 'message';
    const EARLY_BIRD = 'early_bird';
    const GENERAL = 'general';
    const VIP = 'vip';
    const TOKEN = 'token';
    const REGISTRATION_SUCCESS = 'Registration success';
    const UNAUTHORIZED_USER = 'Unauthorized user';
    const NOT_FOUND = 'Not found';
    const EVENT_ID = 'event_id';
    const EVENT_RATING = 'event_rating';
    private $registration_type;

    public function rateEvent(Request $request, AttendeeRegistration $registration)
    {
        try {
            $this->validate($request, [self::EVENT_RATING => 'required|min:0|max:2']);
            if($registration->user_id != $request->user()->id) {
                return response()->json(['message' => 'Unauthorized user'], 401);
            } else {
                $registration->update([self::EVENT_RATING => $request->get(self::EVENT_RATING)]);
                return response()->json(['message' => 'Rating success']);
            }
        } catch (\Throwable $e) {
            return response()->json(['message' => 'Data not valid'],422);
        }
    }

    public function registerForEvent(Request $request)
    {
        $this->registration_type = $request->registration_type;
        try {
            $this->validate($request, [self::EVENT_ID => 'required', 'registration_type' => 'required|in:' . self::EARLY_BIRD . ',' . self::GENERAL . ',' . self::VIP . '']);
            $event = Event::findOrFail($request->get(self::EVENT_ID));
            $user = $request->user();

            $registration = new AttendeeRegistration();
            $registration->event_id = $event->id;
            $registration->user_id = $user->id;
            $registration->registration_type = $this->registration_type;
            switch ($this->registration_type) {
                case self::EARLY_BIRD:
                    $registration->calculated_price = $event->standard_price * 0.85;
                    break;
                case self::VIP:
                    $registration->calculated_price = $event->standard_price * 1.2;
                    break;
                default:
                    $registration->calculated_price = $event->standard_price;
                    break;
            }
            $registration->save();
            return response()->json([self::MESSAGE => self::REGISTRATION_SUCCESS], 200);
        } catch (\Throwable $e) {

            return response()->json([self::MESSAGE => self::NOT_FOUND], 404);
        }
    }

    public function showEvents(Request $request)
    {
        return $request->user()->registrations;
    }
}
