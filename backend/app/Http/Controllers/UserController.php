<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Auth\LoginController;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{

    const TOKEN = 'token';
    const MESSAGE_JSON_STRING = 'message';
    private $loginController;

    /**
     * UserController constructor.
     */
    public function __construct(LoginController $loginController)
    {
        $loginController = $loginController;
    }

    public function register(Request $request)
    {
        DB::beginTransaction();
        $sameEmailUser = User::where('email', $request->email)->get();
        if (count($sameEmailUser) > 0) {
            return response()->json([self::MESSAGE_JSON_STRING => 'User email already registered'], 422);
        } else {
            try {
                $user = new User($request->except('password'));
                $user->password = Hash::make($request['password']);
                $user->token = $this->getMd5Hash($user->username);
                $user->save();
                $file_path = $request->photo->storeAs('public/user-images', $user->id . '.jpg');
                $user->update(['photo' => $file_path]);
                DB::commit();
                return response()->json([
                    self::TOKEN => $user->token,
                ], 200);

            } catch (\Throwable $e) {
                DB::rollBack();
                return response()->json(
                    [self::MESSAGE_JSON_STRING => 'Data can not be processed'], 422
                );
            }
        }
    }

    public function login(Request $request)
    {
        $username = $request->username;
        $password = $request->password;
        $user = User::where([
            'username' => $username
        ])->first();

        if (!empty($user) && Hash::check($password, $user->password)) {
            $user->token = $this->getMd5Hash($user->username);
            $user->save();
            return response()->json([self::TOKEN => $user->token]);
        }

        return response()->json([self::MESSAGE_JSON_STRING => 'invalid login'], 401);
    }

    public function logout(Request $request)
    {
        if ($request->has(self::TOKEN) && !empty($request->get(self::TOKEN))) {
            $user = User::where('token', $request->get('token'))->first();

            if (!empty($user)) {
                $user->token = null;
                $user->save();
            }
        }

        return response()->json([self::MESSAGE_JSON_STRING => 'logout success']);
    }

    /**
     * @return string
     */
    private function getMd5Hash($username): string
    {
        return md5($username);
    }
}
