<?php

namespace App\Http\Middleware;

use App\User;
use Closure;

class CheckUserToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if($request->has('token') && !empty($request->get('token'))) {
            $token = $request->get('token');
            $user = User::where('token', $token)->first();

            if(!empty($user)) {
                $request->setUserResolver(function() use($user) { return $user; });
                return $next($request);
            }
        }

        return response()->json(['message' => 'Unauthorized user'], 401);
    }
}
