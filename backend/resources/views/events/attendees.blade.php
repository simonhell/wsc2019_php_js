@extends('layouts.app')
@section('content')
<div>
    <div class="header">
        <h1>Attendees Events</h1>
        <a class="btn btn-primary" id="add-event" href="/events">Back to events</a>
    </div>
    <div>
        <table>
            <thead>
            <tr>
                <th>Firstname</th>
                <th>Lastname</th>
                <th>Email</th>
                <th>Photo</th>
            </tr>
            </thead>
            <tbody>
            @foreach ($users as $user)
                <tr>
                    <td>{{$user->firstname}}</td>
                    <td>{{$user->lastname}}</td>
                    <td>{{$user->email}}</td>
                    <td><img src="{{$user->photo_url}}" alt="" style="width: 150px;"></td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
</div>
@endsection
