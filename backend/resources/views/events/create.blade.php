@extends('layouts.app')
@section('content')

<div class="header">
    <h1>Create new event</h1>
</div>

<div class="body">
    <form method="POST" action="/events">
        @csrf
        <div class="col-sm-6">
            <label for="title">Title</label>
            <input class="form-control" id="title" type="text" name="title">
        </div>

        <div class="col-sm-6">
            <label for="description">Description</label>
            <textarea class="form-control" id="description" name="description"></textarea>
        </div>

        <div class="col-sm-6">
            <label for="date">Date</label>
            <input class="form-control" id="date" type="date" name="date">
        </div>

        <div class="col-sm-6">
            <label for="time">Time</label>
            <input class="form-control" id="time" type="time" name="time">
        </div>

        <div class="col-sm-6">
            <label for="duration_days">Duration (days)</label>
            <input class="form-control" id="duration_days" type="number" name="duration_days">
        </div>

        <div class="col-sm-6">
            <label for="location">Location</label>
            <input class="form-control" id="location" type="text" name="location">
        </div>

        <div class="col-sm-6">
            <label for="standard_price">Price</label>
            <input class="form-control" id="standard_price" type="number" name="standard_price">
        </div>

        <div class="col-sm-6">
            <label for="capacity">Capacity</label>
            <input class="form-control" id="capacity" type="number" name="capacity">
        </div>

        <div class="col-sm-8">
            <label>Sessions</label>
                <session-component></session-component>
        </div>

        <div class="col-sm-6">
            <button class="btn btn-success" type="submit" id="create-event">
                Create
            </button>
        </div>
    </form>
</div>

@endsection

