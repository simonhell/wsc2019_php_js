@extends('layouts.app')
@section('content')
<div class="header">
    <h1>Event</h1>
</div>
@if (session('message'))
    <span class="text-success text-bold">{{session('message')}}</span>
@endisset
<div class="body">
    <table>
        <tbody>
            <tr>
                <th>Title</th>
                <td class="event-title">{{$event->title}}</td>
            </tr>
            <tr>
                <th>Description</th>
                <td class="event-description">{{$event->description}}</td>
            </tr>
            <tr>
                <th>Date</th>
                <td class="event-date">{{$event->date}}</td>
            </tr>
            <tr>
                <th>Time</th>
                <td class="event-time">{{$event->time}}</td>
            </tr>
            <tr>
                <th>Duration (days)</th>
                <td class="event-duration-days">{{$event->duration_days}}</td>
            </tr>
            <tr>
                <th>Location</th>
                <td class="event-location">{{$event->location}}</td>
            </tr>
            <tr>
                <th>Price</th>
                <td class="event-price">{{$event->standard_price}}</td>
            </tr>
            <tr>
                <th>Capacity</th>
                <td class="event-capacity">{{$event->capacity}}</td>
            </tr>
            <tr>
                <th>Sessions</th>
                <td class="event-sessions">
                            <table>
                                <thead>
                                <tr>
                                    <th>Title</th>
                                    <th>Time</th>
                                    <th>Location</th>
                                    <th>Speaker</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach ($event->sessions as $session)
                                <tr>
                                    <td>{{$session->title}}</td>
                                    <td>{{$session->time}}</td>
                                    <td>{{$session->room}}</td>
                                    <td>{{$session->speaker}}</td>
                                </tr>
                                @endforeach
                                </tbody>
                            </table>
                </td>
            </tr>
        </tbody>
    </table>
</div>
<br><br>
<a class="btn btn-warning" id="edit-event" href="{{route('events.edit', $event->id)}}">Edit event</a>

@endsection
