@extends('layouts.app')
@section('content')
<div class="header">
    Edit event
</div>

<div class="body">
    <form method="POST" action="{{route('events.update', $event->id)}}">
        @csrf
        @method('PUT')
        <div class="col-sm-6">
            <label for="title">Title</label>
            <input class="form-control" id="title" type="text" value="{{$event->title}}" name="title">
        </div>

        <div class="col-sm-6">
            <label for="description">Description</label>
            <textarea class="form-control" id="description"  name="description">{{$event->description}}</textarea>
        </div>

        <div class="col-sm-6">
            <label for="date">Date</label>
            <input class="form-control" id="date" type="date" value="{{$event->date}}" name="date">
        </div>

        <div class="col-sm-6">
            <label for="time">Time</label>
            <input class="form-control" id="time" type="time" value="{{$event->time}}" name="time">
        </div>

        <div class="col-sm-6">
            <label for="duration_days">Duration (days)</label>
            <input class="form-control" id="duration_days" type="number" value="{{$event->duration_days}}" name="duration_days">
        </div>

        <div class="col-sm-6">
            <label for="location">Location</label>
            <input class="form-control" id="location" type="text" name="location" value="{{$event->location}}">
        </div>

        <div class="col-sm-6">
            <label for="standard_price">Price</label>
            <input class="form-control" id="standard_price" type="number" name="standard_price" value="{{$event->standard_price}}">
        </div>

        <div class="col-sm-6">
            <label for="capacity">Capacity</label>
            <input class="form-control" id="capacity" type="number" name="capacity" value="{{$event->capacity}}">
        </div>

        <div class="col-sm-8">
            <label>Sessions</label>
            <div id="edit-sessions" data-sessions="{{$eventSessions}}">
                <session-component></session-component>
            </div>
        </div>

        <div class="col-sm-6">
            <button class="btn btn-success" type="submit" id="create-event">
                Save
            </button>
        </div>
    </form>
</div>
@endsection
