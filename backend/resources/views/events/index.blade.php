@extends('layouts.app')
@section('content')
<div class="header">
    <h1>Events</h1>
    <a class="btn btn-success" id="add-event" href="/events/create">Add event</a>
</div>

<div class="content">
    <table class="table">
        <thead>
            <tr>
                <th>Event</th>
                <th>Date</th>
                <th>Price</th>
                <th>Action</th>
            </tr>
        </thead>
        <tbody>
            @foreach ($events as $event)
            <tr class="event">
                <td class="event-title">
                    <a href="{{route('events.show', ['event' => $event->id])}}">{{$event->title}}</a>
                </td>
                <td class="event-date">{{$event->date}}</td>
                <td class="event-price">{{$event->standard_price}}</td>
                <td class="event-actions">
                    <a class="event-participants" href="{{route('events.attendees', ['event'=> $event->id])}}">Attendee list</a>
                    <a class="event-participants" href="{{route('events.rating', ['event'=> $event->id])}}">Ratings</a>
                </td>
            </tr>
            @endforeach

        </tbody>
    </table>
</div>
@endsection
