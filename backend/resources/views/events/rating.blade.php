@extends('layouts.app')
@section('content')
<div>
    <div class="header">
        <h1>Event Rating</h1>
        <a class="btn btn-primary" id="add-event" href="/events">Back to events</a>
    </div>
    <rating-chart id="ratingChart" data-chart-data="{{$ratings}}"></rating-chart>
</div>
@endsection
