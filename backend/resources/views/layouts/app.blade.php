<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
</head>
<body>
<div id="app" class="container">
    <nav class="navbar navbar-light">
        <div class=" navbar-collapse">

        @guest
        <!-- items for guests -->
        <ul class="navbar-nav">
            <li class="nav-item" class="nav-item">
                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
            </li>
            @if (Route::has('register'))
            <li class="nav-item" class="nav-item">
                <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
            </li>
            @endif
        </ul>

        @else
        <a href="/">
            Event Administration Platform
        </a>
        <!-- items for loggedin user -->
        <ul class="navbar-nav">
            <li class="nav-item">
                <a id="manage-events" href="/events">Manage events</a>
            </li>
            <li class="nav-item">
            <span id="loggedin-user">
                  {{ Auth::user()->username }}
            </span>
            </li>
            <li class="nav-item">
                <a class="dropdown-item" id="logout" href="{{ route('logout') }}"
                   onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                    {{ __('Logout') }}
                </a>

                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                    @csrf
                </form>
            </li>
        </ul>
        @endguest
        </div>

        <!-- /end items for guests -->
    </nav>

    <main class="py-4">
        @yield('content')
    </main>
</div>
</body>
</html>
