<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/


Route::post('v1/profile', 'UserController@register');
Route::post('v1/login', 'UserController@login');

Route::middleware('auth.token')->group(function () {
    Route::get('v1/logout', 'UserController@logout');
    Route::get('v1/events', 'EventController@apiIndex');
    Route::get('v1/registrations', 'RegistrationController@showEvents');
    Route::post('v1/registration', 'RegistrationController@registerForEvent');
    Route::put('v1/registrations/{registration}', 'RegistrationController@rateEvent');
});
