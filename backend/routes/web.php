<?php

use Illuminate\Support\Facades\Auth;

Auth::routes(['register' => false]);

Route::get('/', function() {
    if(Auth::guest()) {
        return view('auth.login');
    } else {
        return redirect('events');
    }
});
Route::resource('/events', 'EventController')->middleware('auth');
Route::get('/events/{event}/attendees', 'EventController@showAttendeeList')->name('events.attendees')->middleware('auth');
Route::get('/events/{event}/rating', 'EventController@showEventRating')->name('events.rating')->middleware('auth');
