import Vue from 'vue'
import VueRouter from 'vue-router'
import Login from '../views/Login.vue'
import Events from '../views/Events.vue'
import Registration from "../views/Registration";
import MyEvents from "../views/MyEvents";
import CreateProfile from "../views/CreateProfile";

import store from '../store';
Vue.use(VueRouter);

const routes = [
    {
        path: '/login',
        name: 'login',
        component: Login
    },
    {
        path: '/',
        name: 'events',
        component: Events
    }, {
        path: '/registration/:id',
        name: 'registration',
        component: Registration
    }, {
        path: '/my-events',
        name: 'my-events',
        component: MyEvents
    }, {
        path: '/create-profile',
        name: 'create-profile',
        component: CreateProfile
    },
]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
});

router.beforeEach(function (to, from, next) {
    if(store.state.loginToken && (to.name === 'login' || to.name === 'create-profile')) {
        console.log('redirect to events', to.name);
        next('/');
    } else if(!store.state.loginToken && to.name !== 'login' && to.name !== 'create-profile') {
        console.log('redirect to login', to.name);
        next('/login');
    } else {
        console.log('normal routing', to.name);
        next();
    }
});

export default router
