import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios';
Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    loginToken: (localStorage.getItem('token')),
    selectedEvent: null,
    events: [],
    registrations: [],
    registerSuccessMessage: null,
  },
  mutations: {
    setLoginToken(state, token) {
      if(token) {
        localStorage.setItem('token', token);
      } else {
        localStorage.removeItem('token');
      }
      state.loginToken = token;
    },
    setEvents(state,events) {
      state.events = events;
    },
    setSelectedEvent(state, event) {
      state.selectedEvent = event;
    },
    setRegistrations(state, registrations) {
      state.registrations = registrations;
    },
    setRegisterSuccessMessage(state, message) {
      state.registerSuccessMessage = message;
    }
  },
  actions: {
    async fetchEvents({commit, state}) {
      const events = await axios.get("http://localhost:8000/api/v1/events?token=" + state.loginToken);
      commit('setEvents', events.data);
    }
  },
  modules: {
  }
})
